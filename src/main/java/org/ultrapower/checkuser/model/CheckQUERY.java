package org.ultrapower.checkuser.model;

import java.io.Serializable;

/**
 * Created by zhaopan on 2017/7/5.<br>
 *
 * @Description:
 */
public class CheckQUERY implements Serializable
{
    private static final long serialVersionUID = 1L;
    private String accountId;//账户信息
    private String phoneNo;//手机号
    private String userName;//姓名
    private String identityCard;//身份证

    public CheckQUERY()
    {
    }

    public CheckQUERY(String phoneNo, String userName, String identityCard)
    {
        this.phoneNo = phoneNo;
        this.userName = userName;
        this.identityCard = identityCard;
    }

    public String getAccountId()
    {
        return accountId;
    }

    public void setAccountId(String accountId)
    {
        this.accountId = accountId;
    }

    public String getPhoneNo()
    {
        return phoneNo;
    }

    public void setPhoneNo(String phoneNo)
    {
        this.phoneNo = phoneNo;
    }

    public String getUserName()
    {
        return userName;
    }

    public void setUserName(String userName)
    {
        this.userName = userName;
    }

    public String getIdentityCard()
    {
        return identityCard;
    }

    public void setIdentityCard(String identityCard)
    {
        this.identityCard = identityCard;
    }
}
