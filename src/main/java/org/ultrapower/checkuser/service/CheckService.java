package org.ultrapower.checkuser.service;

import org.ultrapower.checkuser.core.hbase.HBaseHelper;
import org.ultrapower.checkuser.core.hbase.HbaseInfo;
import org.ultrapower.checkuser.core.helper.XxlsFindByHbaseHelper;
import org.ultrapower.checkuser.core.util.MD5RowKeyGenerator;

import java.io.File;
import java.io.IOException;

/**
 * Created by zhaopan on 2017/7/5.<br>
 *
 * @Description:
 */
public class CheckService
{
    //通过身份证和电话查询用户名
    public static String findUserByIdAndPhone(String phoneNo,String identityCard) throws IOException
    {
        String md5 = new MD5RowKeyGenerator().generatePrefix(phoneNo).toString();
        byte[] userByte= new HBaseHelper(HbaseInfo.hbaseConfig).get(HbaseInfo.TABLE,md5+"|"+phoneNo+"|"+identityCard,HbaseInfo.FAMILY,HbaseInfo.QUALIFIER);
        if(userByte!=null){
            return new String(userByte,"UTF-8");
        }
        return null;
    }

    public static String  BatchCkeckoutUserByFile(File file)throws Exception
    {
        XxlsFindByHbaseHelper howto = new XxlsFindByHbaseHelper();
        //通过Hbase验真文件中的信息，并保存
        return howto.handleUserByIdAndPhone(file);
       // howto.processOneSheet(file.getPath(), 1);
    }
}
