package org.ultrapower.checkuser.web;

import org.ultrapower.checkuser.Constants;
import org.ultrapower.checkuser.core.helper.XxlsFindByHbaseHelper;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.IOException;

/**
 * Created by zhaopan on 2017/7/6.<br>
 *
 * @Description:
 */
@WebServlet("/FileDown")
public class FileDownServlet extends HttpServlet {
    @Override
    protected void service(HttpServletRequest request, HttpServletResponse response) throws IOException {
        String fileName = request.getParameter("fileName");

        XxlsFindByHbaseHelper howto = new XxlsFindByHbaseHelper();

        try {
            howto.fileDownLoad(request, response, new File(Constants.OUTPATH, fileName), fileName);

        } catch (Exception e) {
            e.printStackTrace();
            throw new IOException("下载失败");
        }


    }
}