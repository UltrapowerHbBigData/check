package org.ultrapower.checkuser.web;

import com.alibaba.fastjson.JSONObject;
import org.apache.commons.lang3.StringUtils;
import org.ultrapower.checkuser.HttpCode;
import org.ultrapower.checkuser.service.CheckService;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.Writer;

/**
 * `
 * Created by zhaopan on 2017/7/5.<br>
 *
 * @Description:单条验证
 */
@WebServlet("/SingleAuth")
public class SingleAuthServlet extends HttpServlet
{
    @Override
    protected void service(HttpServletRequest request, HttpServletResponse response) throws IOException
    {
        request.setCharacterEncoding("UTF-8");
        response.setContentType("application/json;charset=utf-8");
        response.setCharacterEncoding("utf-8");
        //设置响应头允许ajax跨域访问,星号表示所有的异域请求都可以接受
        response.setHeader("Access-Control-Allow-Origin", "*");
        response.setHeader("Access-Control-Allow-Methods", "GET,POST");
        JSONObject jsonObject = new JSONObject();
        Writer out = null;
        String accountId = request.getParameter("accountId");
        String phoneNo = request.getParameter("phoneNo");
        String userName = request.getParameter("userName");
        String identityCard = request.getParameter("identityCard");
        try
        {
            out = response.getWriter();
            /*
            put 'USER_INFO','1db|15827611376|420582199005232195','0:NAME','赵攀'
            if (StringUtils.isEmpty(accountId) || !Constants.ACCOUNTID.equals(accountId))
            {
                jsonObject.put("htttCode", HttpCode.FORBIDDEN.value());//403账户信息有误
                jsonObject.put("result", "-1");
                out.write(jsonObject.toString());
                out.flush();
                return;
            } else */if (StringUtils.isEmpty(phoneNo) || StringUtils.isEmpty(userName) || StringUtils.isEmpty(identityCard))
            {
                jsonObject.put("httCode", HttpCode.BAD_REQUEST.value());//400请求参数出错
                jsonObject.put("result", "-1");
                out.write(jsonObject.toString());
                out.flush();
                return;
            } else
            {
                //todo 查询hbase
                String userNameDO = CheckService.findUserByIdAndPhone(phoneNo,identityCard);
                if (StringUtils.isNotEmpty(userNameDO) && userNameDO.equals(userName))
                {
                    jsonObject.put("httCode", HttpCode.OK.value());//200验真成功
                    jsonObject.put("result", "0");
                    out.write(jsonObject.toString());
                    out.flush();
                } else
                {
                    jsonObject.put("httCode", HttpCode.MULTI_STATUS.value());//207验真失败
                    jsonObject.put("result", "999");
                    out.write(jsonObject.toString());
                    out.flush();
                }
            }
        } catch (Exception e)
        {
            e.printStackTrace();
            if (out == null) out = response.getWriter();
            jsonObject.put("htttCode", HttpCode.INTERNAL_SERVER_ERROR.value());//报错
            jsonObject.put("result", "-1");
            out.write(jsonObject.toString());
            out.flush();
        }
    }
}

