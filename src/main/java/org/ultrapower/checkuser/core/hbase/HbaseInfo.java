package org.ultrapower.checkuser.core.hbase;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.hbase.HBaseConfiguration;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Created by zhaopan on 2017/7/5.<br>
 *
 * @Description:Hbase参数
 */
public class HbaseInfo
{
    private static final Logger LOGGER = LoggerFactory.getLogger(HbaseInfo.class);
    public static final Configuration hbaseConfig = HBaseConfiguration.create();
    public static final String TABLE = "USER_INFO";
    public static final String HBASE_USER = "client/hbbdch-dn-01";
    public static final String KEY_PATH = "/home/ultra/client.keytab";
    public static final String FAMILY = "0";
    public static final String QUALIFIER = "NAME";

    static
    {
        //hbaseConfig = HBaseConfiguration.create();
//        hbaseConfig.set("hbase.security.authentication", "kerberos");
//        hbaseConfig.set("hadoop.security.authentication", "Kerberos");
//        System.setProperty("java.security.krb5.realm", "HBBDCH.COM");
//        System.setProperty("java.security.krb5.kdc", "hbbdch-nn-01");
//        hbaseConfig.set("hbase.zookeeper.quorum",
//                "hbbdch-nn-01,hbbdch-nn-02,hbbdch-dn-01");
//        hbaseConfig.set("zookeeper.znode.parent",
//                "/services/slider/users/client/hbase_1007_3");

		hbaseConfig.set("hbase.zookeeper.quorum", "ultra");
		hbaseConfig.set("hbase.zookeeper.property.clientPort", "2181");
		hbaseConfig.setInt("hbase.client.pause", 50);
		hbaseConfig.setInt("hbase.client.retries.number",1);
		hbaseConfig.setInt("hbase.client.scanner.timeout.period",10000);
        hbaseConfig.setInt("hbase.master.catalog.timeout",10000);
        hbaseConfig.setInt("ipc.client.connect.timeout",10000);
     //   hbaseConfig.setInt("zookeeper.session.timeout",10000);
        //hbaseConfig.setInt("zookeeper.session.timeout",10000);
//		hbaseConfig.set("ipc.client.connect.max.retries.on.timeouts", "1");
        hbaseConfig.set("ipc.client.connect.max.retries", "1");
//		hbaseConfig.set("ipc.client.connect.timeout", "5000");
       // hbaseConfig.set("zookeeper.recovery.retry","1");
        //hbase.client.scanner.timeout.period
        //hbase.regionserver.lease.period


//        hbaseConfig.set("ipc.client.coClientCnxnSocketNIOnnection.maxidletime","5000");
        //ipc.client.connection.maxidletime
        //dfs.client.failover.max.attempts
        //ipc.client.connect.timeout
        //ipc.client.connect.max.retries.on.timeouts
        //ipc.client.connect.max.retries
        //hbaseConfig.set("ipc.client.connect.timeout","10000");
		//hbaseConfig.set("hbase.client.operation.timeout", "100");
		//hbaseConfig.set("hbase.client.scanner.timeout.period", "100");
       // UserGroupInformation.setConfiguration(hbaseConfig);
    }
}
