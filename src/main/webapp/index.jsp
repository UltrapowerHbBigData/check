<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>验真</title>
    <link href="static/css/style.css" rel="stylesheet" type="text/css"/>
    <link href="static/css/showLoading.css" rel="stylesheet" type="text/css"/>
    <script type="text/javascript" src="static/js/jquery.min.js"></script>
    <script type="text/javascript" src="static/js/jquery.showLoading.min.js"></script>
    <script type="text/javascript" src="static/js/index.js"></script>
</head>
<body>
<div class="cotn_principal">
    <div class="cont_centrar">
        <div class="cont_login">
            <div class="cont_info_log_sign_up">
                <div class="col_md_login">
                    <div class="cont_ba_opcitiy">
                        <h2>Single</h2>
                        <p>单 个 验 真</p>
                        <button class="btn_login" onclick="cambiar_login()">GET INTO</button>
                    </div>
                </div>
                <div class="col_md_sign_up">
                    <div class="cont_ba_opcitiy">
                        <h2>Batch</h2>
                        <p>批 量 验 真</p>
                        <button class="btn_sign_up" onclick="cambiar_sign_up()">GET INTO</button>
                    </div>
                </div>
            </div>


            <div class="cont_back_info">
                <div class="cont_img_back_grey">
                    <img src="static/po.jpg"
                         alt=""/>
                </div>

            </div>
            <div class="cont_forms">
                <div class="cont_img_back_">
                    <img src="static/po.jpg"
                         alt=""/>
                </div>
                <div class="cont_form_login">
                    <a href="#" onclick="ocultar_login_sign_up()"><img src="static/5-140FG95151.png"/></a>
                    <h2>Single</h2>
                    <input type="text" name="phone" id="phone" placeholder="电话号码"/>
                    <input type="text" name="userName" id="userName" placeholder="姓名"/>
                    <input type="text" name="identity" id="identity" placeholder="身份证"/>
                    <span class="c_red" id="span_username"></span>
                    <input type="text" id="result" placeholder="结果" disabled="false"/>
                    <button class="btn_login" onclick="singleCheck()">CHECK</button>
                </div>

                <div class="cont_form_sign_up">
                    <a href="#" onclick="ocultar_login_sign_up()"><img src="static/5-140FG95151.png"/></a>
                    <h2>Batch</h2>
                    <div class="uploader green">
                        <input type="text" class="filename" id="filename" readonly="readonly"/>
                        <input type="button" name="file" class="button" value="选择文件"/>
                        <input type="file" id="upload" size="30"/>
                    </div>
                    <div></div>
                    <button class="btn_sign_up" onclick="batchCheck()">CHECK</button>
                </div>

            </div>

        </div>
    </div>
</div>
</body>
<script>
    //获得焦点后border恢复默认
    $("input[type='text']").focus(function () {
        $(this).css("border", "none");
    });
    //显示选择的文件名
    $("input[type=file]").change(function () {
        $(this).parents(".uploader").find(".filename").val($(this).val());
    });
    //单个验证
    function singleCheck() {
        if (!isPhoneNo($("#phone").val())) {
            $("#phone").css("border", "1px solid red");
            return;
        }
        if (!isChinaName($("#userName").val())) {
            $("#userName").css("border", "1px solid red");
            return;
        }
        if (!isCardNo($("#identity").val())) {
            $("#identity").css("border", "1px solid red");
            return;
        }
        $.ajax({
            url: '/SingleAuth',
            type: 'post',
            dataType: 'json',
            dataType: "json",
            data: {phoneNo: $("#phone").val(), userName: $("#userName").val(), identityCard: $("#identity").val()},
            timeout: 10000,
            beforeSend: function () {
                //console.log("正在进行，请稍候");
                $("body").showLoading();
            },
            success: function (data, status) {
                if (data.httCode == 200) {
                    $("#result").val("通过").css('color', '#F00');
                } else {
                    $("#result").val("不通过").css('color', '#F00');
                }
                $("body").hideLoading();
            },
            error: function (err, status) {
                $("body").hideLoading();
                alert("查询失败");
            }
        });
    }
    //批量验证
    function batchCheck() {
        var formData = new FormData();
        var name = $("#filename").val();
        formData.append("file", $("#upload")[0].files[0]);
        //formData.append("name", name);
        $.ajax({
            url: "/BatchAuth",
            type: 'POST',
            data: formData,
            // 告诉jQuery不要去处理发送的数据
            processData: false,
            // 告诉jQuery不要去设置Content-Type请求头
            contentType: false,
            beforeSend: function () {
                //console.log("正在进行，请稍候");
                $("body").showLoading();
            },
            success: function (returnValue, status) {
                $("body").hideLoading();
                if (returnValue.httCode == 200) {
                    window.location.href = "/FileDown?fileName=" + returnValue.data;
                }
            },
            error: function (responseStr) {
                $("body").hideLoading();
                alert("操作失败");
            }
        });

    }
    // 验证身份证
    function isCardNo(card) {
        var pattern = /(^\d{15}$)|(^\d{18}$)|(^\d{17}(\d|X|x)$)/;
        return pattern.test(card);
    }
    // 验证姓名
    function isChinaName(name) {
        var pattern = /^[\u4E00-\u9FA5]{2,6}$/;
        return pattern.test(name);
    }
    // 验证手机号
    function isPhoneNo(phone) {
        var pattern = /^1[34578]\d{9}$/;
        return pattern.test(phone);
    }
</script>
</html>